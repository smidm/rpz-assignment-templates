% RPZ assigment: Logistic regression


%% Init
run('<path_to_stprtool>/stprpath.m');

%% Classification of letters A and C
%--------------------------------------------------------------------------
% Load training data and compute features
load ('data_33rpz_logreg.mat');

% Prepare the training data
trainX = ???;

% Training - gradient descent of the logistic loss function
% Start at a fixed point:
w_init = [-7; -8];
% or start at a random point:
% w_init = 20 * (rand(size(trainX, 1), 1) - 0.5);
epsilon = 1e-2;
[w, wt, Et] = logistic_loss_gradient_descent(trainX, trn.labels, w_init, epsilon);

% Plot the progress of the gradient descent
% plotEt
???;
    
%
plot_gradient_descent(trainX, trn.labels, @logistic_loss, w, wt, Et);

% plot aposteriori probabilities
thr = get_threshold(w);
...

% Prepare the test data
testX = ???;
    
% Classify letter test data and calculate classification error
classifiedLabels = classify_images(testX, w);

testError = ???;
fprintf('Letter classification error: %.2f%%\n', testError * 100);

% Visualize classification results
show_classification(tst.images, classifiedLabels, 'AC');


%% Classification of MNIST digits
%--------------------------------------------------------------------------
% Load training data
load('mnist_01_trn');

% prepare the training data
Xtrain = ???;
y = ???;

% Training - gradient descent of the logistic loss function
w_init = rand(size(X, 1), 1);
epsilon = 1e-2;
[w, ~, Et] = logistic_loss_gradient_descent(Xtrain, y, w_init, epsilon);

% Plot the progress of the gradient descent
% plotEt
???;
    
% Load test data
load('mnist_01_tst');
Xtest = ???;

% prepare the training data

% Classify MNIST test data and calculate classification error
classifiedLabels = classify_images(Xtest, w);
testError = ???;
fprintf('MNIST digit classification error: %.2f%%\n', testError * 100);

% Visualize classification results
show_mnist_classification(Xtest(2:end, :), classifiedLabels);
