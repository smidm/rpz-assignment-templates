% Assigment: PARZEN WINDOW ESTIMATION

%% init
load data_33rpz_parzen.mat
run('../../../../solutions/3rdparty/stprtool/stprpath.m')

%% Tasks, part 1
%% measurements
x = compute_measurement_lr_cont(trn.images);

% splitting the trainning data into classes
xA = !!FIXME!!
xC = !!FIXME!!

%% computing the histograms of training data
[bins_A centers_A] = hist(xA, 20);
bins_A = bins_A / (sum(bins_A)*(centers_A(2)-centers_A(1)));

[bins_C centers_C] = hist(xC);
bins_C = bins_C / (sum(bins_C)*(centers_C(2)-centers_C(1)));

%% estimating conditional probability using Parzen window
x_range = min(xA):100:max(xA);
h = [100 500 1000 2000];
p = !!FIXME!!

%% plots of the estimates
figure;
% h = 100
subplot(2,2,1);
bar(centers_A, bins_A);
hold on;
plot(x_range, !!FIXME!!, 'r', 'linewidth', 2);
title('h = 100');
xlabel('x');
ylabel('p(x|A)');
ylim([0 4.5e-4]);
grid on;
% h = 300
subplot(2,2,2);
bar(centers_A, bins_A);
hold on;
plot(x_range, !!FIXME!!, 'r', 'linewidth', 2);
title('h = 500');
xlabel('x');
ylabel('p(x|A)');
ylim([0 4.5e-4]);
grid on;
% h = 1000
subplot(2,2,3);
bar(centers_A, bins_A);
hold on;
plot(x_range, !!FIXME!!, 'r', 'linewidth', 2);
title('h = 1000');
xlabel('x');
ylabel('p(x|A)');
ylim([0 4.5e-4]);
grid on;
% h = 3000
subplot(2,2,4);
bar(centers_A, bins_A);
hold on;
plot(x_range, !!FIXME!!, 'r', 'linewidth', 2);
title('h = 2000');
xlabel('x');
ylabel('p(x|A)');
ylim([0 4.5e-4]);
grid on;

%% Tasks, part 2
%% 10-fold cross-validation init
h_range = 100:50:1000;
num_folds = 10;

%% class A cross-validation
rand('seed', 42);   % needed only for upload system, to test the correctness of the code

Lh = !!FIXME!!

%% optimal value of parameter h
h_bestA = !!FIXME!!
Lh_bestA = !!FIXME!!

%% plots of optimal h
figure;
subplot(1,2,1);
plot(h_range, Lh);
hold on;
plot(h_bestA, Lh_bestA,'or');
cur_ylim = get(gca, 'ylim');
plot([h_bestA h_bestA], [cur_ylim(1) Lh_bestA], '--r');
title('10-fold cross-validation');
xlabel('h');
ylabel('L(h)');
grid on;

p = !!FIXME!!
subplot(1,2,2);
bar(centers_A, bins_A);
hold on;
plot(x_range, p, 'r', 'linewidth', 2);
grid on;
title('Best bandwidth h for class A');
xlabel('x');
ylabel('p(x|A)');

%% class C cross-validation
rand('seed', 42);   % needed only for upload system, to test the correctness of the code

Lh = !!FIXME!!

%% optimal value of parameter h
h_bestC = !!FIXME!!
Lh_bestC = !!FIXME!!

%% plots of optimal h
figure;
subplot(1,2,1);
plot(h_range, Lh);
hold on;
plot(h_bestC, Lh_bestC,'or');
title('10-fold cross-validation');
xlabel('h');
ylabel('L(h)');
grid on;

p = !!FIXME!!
subplot(1,2,2);
bar(centers_C, bins_C);
hold on;
plot(x_range, p, 'r', 'linewidth', 2);
grid on;
title('Best bandwidth h for class C');
xlabel('x');
ylabel('p(x|C)');

%% Bayesian classifier
x_test = compute_measurement_lr_cont(tst.images);

% computing a priori probabilities
pA = !!FIXME!!
pC = !!FIXME!!

labels = classify_bayes_parzen(!!FIXME!!);

% showing images
show_classification(tst.images, labels, 'AC'); 

% classification error
bayes_error = !!FIXME!!
