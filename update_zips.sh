#!/usr/bin/env bash

set -eu

export GIT_WORK_TREE=/home/tom/th_data/cmp/teaching/rpz_2017/assignment_templates
ZIP_OUT_DIR='/run/user/1000/gvfs/sftp:host=cw.felk.cvut.cz,user=hodanto2/www/pages/courses/RPZ/assignment_templates'

cd $GIT_WORK_TREE
git pull origin master
rm $ZIP_OUT_DIR/assignment_*_template.zip
find $GIT_WORK_TREE -type f -name '*~' -delete

for DIR in $GIT_WORK_TREE/assignment_*_template ; do
    if [ -d $DIR ]; then
        echo $DIR;
        zip -rjq $ZIP_OUT_DIR/`basename $DIR`.zip $DIR ;
    fi
done
chmod o+rw $ZIP_OUT_DIR/*.zip

echo 'Updated zip files'

